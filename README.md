# Intro

[![Join the chat at](https://img.shields.io/discord/584773460585086977?label=discord)](https://discord.com/invite/cnUXsws)

Hello, nice to meet you here. Glad to introduce you **CrocOTT** platform.
**CrocOTT** is a platform for self-hosted or cloud based video streaming/hosting solution, customized according your vision.

# Why do you need it?
- You want to sell your video/audio content for your clients
- You need to be in touch with your clients and have your own community
- You don't like cloud video based solutions (You need freedom and independence)
- You have your own brand and want to protect your unique content

This platform contains 4 parts:
1) Package interface (**Opensource**): https://gitlab.com/fastocloud/wsfastocloud
2) Package backend: https://gitlab.com/fastocloud/gofastocloud_backend
3) Panel for managing Clients/Providers/Packages: https://gitlab.com/fastogt/fastocloud_ott/frontend
4) Backend/Load balance to handle clients/providers requests: https://gitlab.com/fastogt/fastocloud_ott/gobackend
5) Players for watching and selling content: https://gitlab.com/fastogt/fastocloud_ott

Every part can be customized.

## Examples
* Racing
<img src="https://gitlab.com/fastogt/fastocloud_ott/docs/-/raw/main/images/race_live.jpg" width="500">

* Online theater
<img src="https://gitlab.com/fastogt/fastocloud_ott/docs/-/raw/main/images/mortal.png" width="500">

* English online school
<img src="https://gitlab.com/fastogt/fastocloud_ott/docs/-/raw/main/images/british_cat.png" width="500">

* Video hosting
<img src="https://gitlab.com/fastogt/fastocloud_ott/docs/-/raw/main/images/vods_logo.jpg" width="500">
<img src="https://gitlab.com/fastogt/fastocloud_ott/docs/-/raw/main/images/android_mobile/walk.png" width="200">

* IPTV
<img src="https://gitlab.com/fastogt/fastocloud_ott/docs/-/raw/main/images/android_tv/2stream_vod.png" width="500">
<img src="https://gitlab.com/fastogt/fastocloud_ott/docs/-/raw/main/images/android_tv/2stream_vod_desc.png" width="500">


## Demo Players
### CrocOTT Layout:
* **Web:** http://crocott.crocott.com or https://crocott.crocott.com for login: **test@crocott.com 1111** or by code **076534434731**
* **Android:** https://play.google.com/store/apps/details?id=com.crocott for login: **test@crocott.com 1111** or by code **076534434731**
* **Apple:** https://apps.apple.com/us/app/crocott/id1642691103 for login: **test@crocott.com 1111** or by code **076534434731**
* **Roku:** https://my.roku.com/account/add/22T6PHG for login: **test@crocott.com 1111** 
* **LG:** we can send ipk file, in publishing to store now
* **Samsung:** in development now, available for test from November 2024

### TurtleOTT Layout:
* **Android:** https://play.google.com/store/apps/details?id=com.turtleott for login: **test@crocott.com 1111**  or by code **076534434731**

### PythonOTT Layout:
* **Web:** http://pythonott.crocott.com or https://pythonott.crocott.com for login: **test@crocott.com 1111**  or by code **076534434731**
* **Android:** https://play.google.com/store/apps/details?id=com.pythonott for login: **test@crocott.com 1111**  or by code **076534434731**

### RaptorOTT Layout:
* **iPhone/iPad:** https://apps.apple.com/us/app/raptorott/id6472151479 for login: **test@crocott.com 1111**

### VenomOTT Layout:
* **iPhone/iPad:** https://testflight.apple.com/join/QvkH6dSB for login: **test@crocott.com 1111**
* **Android:** https://fastogt.com/static/projects/venomott.apk for login: **test@crocott.com 1111**

## Demo Panel
* **Panel:** https://ott.crocott.com

## Presentations
* https://fastogt.com/static/projects/all_presentations/crocott_presentation.pdf

## API (Documentation)
* https://fastogt.stoplight.io/docs/crocott/reference/crocott.yaml

## Let's get started!
https://gitlab.com/fastogt/fastocloud_ott/docs/-/wikis/FastoCloud-OTT-how-to-use%3F
