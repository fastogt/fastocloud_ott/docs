#!/bin/bash
set -x

timestamp=$(date +%s)
mongodump --db=ott --archive --gzip | cat > $timestamp.gz