#!/bin/bash
set -x

## exports
export PKG_CONFIG_PATH=$PKG_CONFIG_PATH:/usr/local/lib/pkgconfig

# variables
USER=fastocloud

# update system
if [[ "$OSTYPE" == "linux-gnu" ]]; then
    if [ -n "$(command -v yum)" ]; then
      yum update -y
      yum install -y git ca-certificates python3-setuptools python3-pip
      yum install docker.io mongodb nginx
    elif [ -n "$(command -v apt-get)" ]; then
      apt-get update # apt-get update --allow-insecure-repositories
      apt-get install -y ca-certificates git python3-setuptools python3-pip python3-certbot-nginx --no-install-recommends
      apt-get -y install nginx
# wget -qO- https://pgp.mongodb.com/server-7.0.asc | gpg --dearmor | tee /usr/share/keyrings/mongodb-server-7.0.gpg >/dev/null
# echo "deb [ arch=amd64,arm64 signed-by=/usr/share/keyrings/mongodb-server-7.0.gpg ] https://repo.mongodb.org/apt/ubuntu $(lsb_release -cs)/mongodb-org/7.0 multiverse" | tee -a /etc/apt/sources.list.d/mongodb-org-7.0.list
      apt-get -y install mongodb
      apt-get -y install docker.io
    else
:
    fi
elif [[ "$OSTYPE" == "darwin"* ]]; then
  brew update
  brew install git ca-certificates brew-pip
  pip3 install -U pip setuptools certifi
:
elif [[ "$OSTYPE" == "cygwin" ]]; then
:
elif [[ "$OSTYPE" == "msys" ]]; then
  pacman -Suy --noconfirm ca-certificates git python3-setuptools python3-pip
:
elif [[ "$OSTYPE" == "win32" ]]; then
:
elif [[ "$OSTYPE" == "freebsd"* ]]; then
:
else
:
fi

# install pyfastogt
git clone https://gitlab.com/fastogt/pyfastogt
cd pyfastogt
python3 setup.py install
cd ../
rm -rf pyfastogt

# add user
useradd -m -U -d /home/$USER $USER -s /bin/bash
# init docker
usermod -aG docker fastocloud
chmod 666 /var/run/docker.sock
# get gofastocloud_backend image
docker pull fastocloud/gofastocloud_backend:latest