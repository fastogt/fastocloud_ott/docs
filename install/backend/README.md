## Prerequisite:

- MongoDB instance
- Docker for local packages

here good script how to build on clean machine: https://gitlab.com/fastogt/fastocloud_ott/docs/-/raw/main/install/backend/build_env.sh

`wget -O - https://gitlab.com/fastogt/fastocloud_ott/docs/-/raw/main/install/backend/build_env.sh | bash`

after script finished need to setup CrocOTT service, here you can find `crocott` installation package: https://fastocloud.com/downloads.html

for example steps:

```
dpkg -i crocott-1.0.0-x86_64-1a64c6ca.deb
systemctl enable crocott.service
systemctl start crocott.service
```

for nginx:

`wget https://gitlab.com/fastogt/fastocloud_ott/docs/-/raw/main/install/backend/nginx/crocott && cp crocott /etc/nginx/sites-enabled/default && gpasswd -a www-data fastocloud && systemctl restart nginx`
